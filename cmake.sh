#!/bin/bash

#Creamos el directorio donde compilaremos el código
mkdir -p ./compiled
cd compiled

#Compilamos
cmake ..

#Incluimos la opcion CMAKE_BUILD_TYPE=Release
echo -n "" > CMakeCache_temp.txt
while read line
			do 
				echo -n -e "$line" >> CMakeCache_temp.txt
				if test "${line#*'CMAKE_BUILD_TYPE:STRING='}" != "$line"
					then	# Es la linea que queremos cambiar
						echo -e "Release" >> CMakeCache_temp.txt
					else	#Es cualquier otra linea
						echo -e "" >> CMakeCache_temp.txt
				fi
			done < ./CMakeCache.txt
rm -f CMakeCache.txt
mv -f CMakeCache_temp.txt CMakeCache.txt

#Volvemos a compilar con el nuevo
cmake ..
make