#!/bin/bash
# Se calcula la media de tiempo de los archivos de resultados

#El -p evita que se imprima un error si el directorio ya existe.
mkdir -p ./res/avg

#------------------------------------------------------------------------------------------------------------------------------------------
#Array of Structs - AOS
#------------------------------------------------------------------------------------------------------------------------------------------
echo -n -e "Extracting AOS data..."
echo -e "Resultados de AOS" > ./res/avg/aos_results.txt
echo -e "----------------------------------------------------------------------------------------------\n" >> ./res/avg/aos_results.txt

	#-------------------------------------------------------------------------------------------------------------------------------
	# Locking to default iterations (100)
	#-------------------------------------------------------------------------------------------------------------------------------
	echo -e "\tManteniendo las iteraciones por defecto (100)" >> ./res/avg/aos_results.txt
	echo -e "\t-------------------------------------------------------------------------------" >> ./res/avg/aos_results.txt
	
		#----------------------------------------------------------------------------------------------------
		# 250 bodies file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 250 cuerpos" >> ./res/avg/aos_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/aos_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/aos_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/aos_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_100i.txt
		echo -e "\n" >> ./res/avg/aos_results.txt

		#----------------------------------------------------------------------------------------------------
		# 500 bodies file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 500 cuerpos" >> ./res/avg/aos_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/aos_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/aos_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/aos_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_500b_100i.txt
		echo -e "\n" >> ./res/avg/aos_results.txt

		#----------------------------------------------------------------------------------------------------
		# 750 bodies file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 750 cuerpos" >> ./res/avg/aos_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/aos_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/aos_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/aos_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_750b_100i.txt
		echo -e "\n" >> ./res/avg/aos_results.txt

		#----------------------------------------------------------------------------------------------------
		# 1000 bodies file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 1000 cuerpos" >> ./res/avg/aos_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/aos_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/aos_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/aos_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_1000b_100i.txt
		echo -e "\n" >> ./res/avg/aos_results.txt

	#-------------------------------------------------------------------------------------------------------------------------------
	# Locking to default bodies (250)
	#-------------------------------------------------------------------------------------------------------------------------------
	echo -e "\tManteniendo los cuerpos por defecto (250)" >> ./res/avg/aos_results.txt
	echo -e "\t-------------------------------------------------------------------------------" >> ./res/avg/aos_results.txt
	
		#----------------------------------------------------------------------------------------------------
		# 50 iterations file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 50 iteraciones" >> ./res/avg/aos_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/aos_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/aos_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/aos_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_50i.txt
		echo -e "\n" >> ./res/avg/aos_results.txt

		#----------------------------------------------------------------------------------------------------
		# 100 iterations file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 100 iteraciones" >> ./res/avg/aos_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/aos_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/aos_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/aos_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_100i.txt
		echo -e "\n" >> ./res/avg/aos_results.txt

		#----------------------------------------------------------------------------------------------------
		# 150 iterations file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 150 iteraciones" >> ./res/avg/aos_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/aos_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/aos_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/aos_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_150i.txt
		echo -e "\n" >> ./res/avg/aos_results.txt

		#----------------------------------------------------------------------------------------------------
		# 200 iterations file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 200 iteraciones" >> ./res/avg/aos_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/aos_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/aos_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/aos_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_200i.txt
		echo -e "\n" >> ./res/avg/aos_results.txt

echo -e " done!"

#------------------------------------------------------------------------------------------------------------------------------------------
#Struct of Arrays - SOA
#------------------------------------------------------------------------------------------------------------------------------------------
echo -n -e "Extracting SOA data..."
echo -e "Resultados de SOA" > ./res/avg/soa_results.txt
echo -e "----------------------------------------------------------------------------------------------\n" >> ./res/avg/soa_results.txt

	#-------------------------------------------------------------------------------------------------------------------------------
	# Locking to default iterations (100)
	#-------------------------------------------------------------------------------------------------------------------------------
	echo -e "\tManteniendo las iteraciones por defecto (100)" >> ./res/avg/soa_results.txt
	echo -e "\t-------------------------------------------------------------------------------" >> ./res/avg/soa_results.txt
	
		#----------------------------------------------------------------------------------------------------
		# 250 bodies file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 250 cuerpos" >> ./res/avg/soa_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/soa_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/soa_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/soa_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_100i.txt
		echo -e "\n" >> ./res/avg/soa_results.txt

		#----------------------------------------------------------------------------------------------------
		# 500 bodies file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 500 cuerpos" >> ./res/avg/soa_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/soa_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/soa_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/soa_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_500b_100i.txt
		echo -e "\n" >> ./res/avg/soa_results.txt

		#----------------------------------------------------------------------------------------------------
		# 750 bodies file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 750 cuerpos" >> ./res/avg/soa_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/soa_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/soa_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/soa_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_750b_100i.txt
		echo -e "\n" >> ./res/avg/soa_results.txt

		#----------------------------------------------------------------------------------------------------
		# 1000 bodies file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 1000 cuerpos" >> ./res/avg/soa_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/soa_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/soa_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/soa_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_1000b_100i.txt
		echo -e "\n" >> ./res/avg/soa_results.txt

	#-------------------------------------------------------------------------------------------------------------------------------
	# Locking to default bodies (250)
	#-------------------------------------------------------------------------------------------------------------------------------
	echo -e "\tManteniendo los cuerpos por defecto (250)" >> ./res/avg/soa_results.txt
	echo -e "\t-------------------------------------------------------------------------------" >> ./res/avg/soa_results.txt
	
		#----------------------------------------------------------------------------------------------------
		# 50 iterations file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 50 iteraciones" >> ./res/avg/soa_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/soa_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/soa_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/soa_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_50i.txt
		echo -e "\n" >> ./res/avg/soa_results.txt

		#----------------------------------------------------------------------------------------------------
		# 100 iterations file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 100 iteraciones" >> ./res/avg/soa_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/soa_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/soa_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/soa_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_100i.txt
		echo -e "\n" >> ./res/avg/soa_results.txt

		#----------------------------------------------------------------------------------------------------
		# 150 iterations file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 150 iteraciones" >> ./res/avg/soa_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/soa_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/soa_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/soa_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_150i.txt
		echo -e "\n" >> ./res/avg/soa_results.txt

		#----------------------------------------------------------------------------------------------------
		# 200 iterations file
		#----------------------------------------------------------------------------------------------------
		echo -e "\t\tCon 200 iteraciones" >> ./res/avg/soa_results.txt
		echo -e "\t\t-----------------------------------------------------------------" >> ./res/avg/soa_results.txt
		prueba=$"1"
		while read line
			do 
				param="Time: "
				if test "${line#*'Time: '}" != "$line"
					then	# La linea contiene la linea con el tiempo.
						echo -n -e "\t\t\t$prueba.\t" >> ./res/avg/soa_results.txt
						echo $line | cut -d " " -f2- | sed 's/\./,/' >> ./res/avg/soa_results.txt
						prueba=$((prueba+1))
				fi
			done < ./res/eval/aos/test_aos_250b_200i.txt
		echo -e "\n" >> ./res/avg/soa_results.txt

echo -e " done!"