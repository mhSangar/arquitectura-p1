#!/bin/bash
# Conjunto de pruebas para el ejercicio NBodies.

mkdir -p ./res
mkdir -p ./res/aos
mkdir -p ./res/soa

#------------------------------------------------------------------------------------------------------------------------------------------
#Array of Structs - AOS
#------------------------------------------------------------------------------------------------------------------------------------------

echo -e "\nEvaluating AOS... "

	#---------------------------------------------------------------------------------------------------------------
	echo -e "\tFor 50 iterations"
	#---------------------------------------------------------------------------------------------------------------
		echo -n -e "\t\tFor 250 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 250 50 > ./res/aos/test_aos_250b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_250b_50i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 250 50 >> ./res/aos/test_aos_250b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_250b_50i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 500 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 500 50 > ./res/aos/test_aos_500b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_500b_50i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 500 50 >> ./res/aos/test_aos_500b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_500b_50i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

	 	echo -n -e "\t\tFor 750 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 750 50 > ./res/aos/test_aos_750b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_750b_50i.txt			
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 750 50 >> ./res/aos/test_aos_750b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_750b_50i.txt			
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 1000 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 1000 50 > ./res/aos/test_aos_1000b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_1000b_50i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 1000 50 >> ./res/aos/test_aos_1000b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_1000b_50i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

	#---------------------------------------------------------------------------------------------------------------
	echo -e "\tFor 100 iterations"
	#---------------------------------------------------------------------------------------------------------------
		echo -n -e "\t\tFor 250 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 250 100 > ./res/aos/test_aos_250b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_250b_100i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 250 100 >> ./res/aos/test_aos_250b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_250b_100i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 500 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 500 100 > ./res/aos/test_aos_500b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_500b_100i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 500 100 >> ./res/aos/test_aos_500b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_500b_100i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 750 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 750 100 > ./res/aos/test_aos_750b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_750b_100i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 750 100 >> ./res/aos/test_aos_750b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_750b_100i.txt
							echo -n -e "."
					fi
				done			
		echo -e " done!"

		echo -n -e "\t\tFor 1000 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 1000 100 > ./res/aos/test_aos_1000b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_1000b_100i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 1000 100 >> ./res/aos/test_aos_1000b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_1000b_100i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

	#---------------------------------------------------------------------------------------------------------------
	echo -e "\tFor 150 iterations"
	#---------------------------------------------------------------------------------------------------------------
		echo -n -e "\t\tFor 250 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 250 150 > ./res/aos/test_aos_250b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_250b_150i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 250 150 >> ./res/aos/test_aos_250b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_250b_150i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 500 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 500 150 > ./res/aos/test_aos_500b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_500b_150i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 500 150 >> ./res/aos/test_aos_500b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_500b_150i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 750 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 750 150 > ./res/aos/test_aos_750b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_750b_150i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 750 150 >> ./res/aos/test_aos_750b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_750b_150i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"


		echo -n -e "\t\tFor 1000 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 1000 150 > ./res/aos/test_aos_1000b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_1000b_150i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 1000 150 >> ./res/aos/test_aos_1000b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_1000b_150i.txt
							echo -n -e "."
					fi
				done			
		echo -e " done!"

	#---------------------------------------------------------------------------------------------------------------
	echo -e "\tFor 200 iterations"
	#---------------------------------------------------------------------------------------------------------------
		echo -n -e "\t\tFor 250 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 250 200 > ./res/aos/test_aos_250b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_250b_200i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 250 200 >> ./res/aos/test_aos_250b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_250b_200i.txt
							echo -n -e "."
					fi
				done			
		echo -e " done!"

		echo -n -e "\t\tFor 500 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 500 200 > ./res/aos/test_aos_500b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_500b_200i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 500 200 >> ./res/aos/test_aos_500b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_500b_200i.txt
							echo -n -e "."
					fi
				done 
		echo -e " done!"

		echo -n -e "\t\tFor 750 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 750 200 > ./res/aos/test_aos_750b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_750b_200i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 750 200 >> ./res/aos/test_aos_750b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_750b_200i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 1000 bodies"
	 		for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 1000 200 > ./res/aos/test_aos_1000b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_1000b_200i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 1000 200 >> ./res/aos/test_aos_1000b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/aos/test_aos_1000b_200i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

#------------------------------------------------------------------------------------------------------------------------------------------
#Struct of Arrays - SOA
#------------------------------------------------------------------------------------------------------------------------------------------
echo -e "\nEvaluating SOA..."

	#---------------------------------------------------------------------------------------------------------------
	echo -e "\tFor 50 iterations"
	#---------------------------------------------------------------------------------------------------------------
		echo -n -e "\t\tFor 250 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 250 50 > ./res/soa/test_soa_250b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_250b_50i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 250 50 >> ./res/soa/test_soa_250b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_250b_50i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 500 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 500 50 > ./res/soa/test_soa_500b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_500b_50i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 500 50 >> ./res/soa/test_soa_500b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_500b_50i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 750 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 750 50 > ./res/soa/test_soa_750b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_750b_50i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 750 50 >> ./res/soa/test_soa_750b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_750b_50i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 1000 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 1000 50 > ./res/soa/test_soa_1000b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_1000b_50i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 1000 50 >> ./res/soa/test_soa_1000b_50i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_1000b_50i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

	#---------------------------------------------------------------------------------------------------------------
	echo -e "\tFor 100 iterations"
	#---------------------------------------------------------------------------------------------------------------
		echo -n -e "\t\tFor 250 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 250 100 > ./res/soa/test_soa_250b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_250b_100i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 250 100 >> ./res/soa/test_soa_250b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_250b_100i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 500 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 500 100 > ./res/soa/test_soa_500b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_500b_100i.txt
							echo -n -e "."
					else
							./compiled/nbodyrnd aos 500 100 >> ./res/soa/test_soa_500b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_500b_100i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 750 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 750 100 > ./res/soa/test_soa_750b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_750b_100i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 750 100 >> ./res/soa/test_soa_750b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_750b_100i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 1000 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 1000 100 > ./res/soa/test_soa_1000b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_1000b_100i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 1000 100 >> ./res/soa/test_soa_1000b_100i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_1000b_100i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"


	#---------------------------------------------------------------------------------------------------------------
	echo -e "\tFor 150 iterations"
	#---------------------------------------------------------------------------------------------------------------
		echo -n -e "\t\tFor 250 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 250 150 > ./res/soa/test_soa_250b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_250b_150i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 250 150 >> ./res/soa/test_soa_250b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_250b_150i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 500 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 500 150 > ./res/soa/test_soa_500b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_500b_150i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 500 150 >> ./res/soa/test_soa_500b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_500b_150i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 750 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 750 150 > ./res/soa/test_soa_750b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_750b_150i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 750 150 >> ./res/soa/test_soa_750b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_750b_150i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 1000 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 1000 150 > ./res/soa/test_soa_1000b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_1000b_150i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 1000 150 >> ./res/soa/test_soa_1000b_150i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_1000b_150i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

	#---------------------------------------------------------------------------------------------------------------
	echo -e "\tFor 200 iterations"
	#---------------------------------------------------------------------------------------------------------------
		echo -n -e "\t\tFor 250 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 250 200 > ./res/soa/test_soa_250b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_250b_200i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 250 200 >> ./res/soa/test_soa_250b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_250b_200i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 500 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 500 200 > ./res/soa/test_soa_500b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_500b_200i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 500 200 >> ./res/soa/test_soa_500b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_500b_200i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 750 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 750 200 > ./res/soa/test_soa_750b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_750b_200i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 750 200 >> ./res/soa/test_soa_750b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_750b_200i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

		echo -n -e "\t\tFor 1000 bodies"
			for i in 1
				do
					if [ "$i" -eq "1" ]
						then
							./compiled/nbodyrnd aos 1000 200 > ./res/soa/test_soa_1000b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_1000b_200i.txt
							echo -n -e "."
						else
							./compiled/nbodyrnd aos 1000 200 >> ./res/soa/test_soa_1000b_200i.txt
							echo -e "\n----------------------------------------------------------\n" >> ./res/soa/test_soa_1000b_200i.txt
							echo -n -e "."
					fi
				done
		echo -e " done!"

#------------------------------------------------------------------------------------------------------------------------------------------
#END
#------------------------------------------------------------------------------------------------------------------------------------------
echo -e "\nEvaluation done!\n"
